import React, {useEffect, useState} from 'react';
import {DataTable, HeaderInput} from '../../components/index';
import {BinarySearchTree as BST} from '../../utils/bst';
import {searchFilds} from '../../constants/data/search_fields';
import {addToLocalStorage} from "../../utils/handle_local_storage";
import {useLocation} from 'react-router-dom'
import './_style.scss';



const data = require('../../data/data.json');

const Home = () => {

    const location = useLocation();
    const [bst, setBST] = useState(new BST());
    const [usersData, setState] = useState(data);
    const [filters, setFilters] = useState(searchFilds);
    const [order, setOrder] = useState({"order": 1, "sortType": "id"});
    const [bookmark, setBookmark] = useState(localStorage.getItem('bookmark') ? JSON.parse(localStorage.getItem('bookmark')) : []);



    useEffect(() => {
        usersData.forEach((item) => {
            const itemDate = new Date(item.date).setHours(0, 0, 0, 0);
            bst.insert(itemDate, item);
        });

        let searchParams = new URLSearchParams(location.search);
        const userName = searchParams.get('name')
            ? searchParams.get('name')
            : '';
        const changer = searchParams.get('changer')
            ? searchParams.get('changer')
            : '';
        const field = searchParams.get('field')
            ? searchParams.get('field')
            : '';

        setFilters((prevState) => ({...prevState, userName, changer: changer, field}));
    }, []);


    useEffect(() => {
        const url = new URL(window.location.href);
        url.searchParams.set('name', filters.userName ? filters.userName : '');
        url.searchParams.set(
            'changer',
            filters.changer ? filters.changer : ''
        );
        url.searchParams.set('field', filters.field ? filters.field : '');
        window.history.replaceState(null, null, url);
        let filterData = data.filter((item) => {
            return (
                item.name?.toLowerCase().includes(filters.userName.toLowerCase()) &&
                item.field?.toLowerCase().includes(filters.field.toLowerCase()) &&
                item.title?.toLowerCase().includes(filters.changer.toLowerCase())
            );
        });

        setState(filterData);
        setFilters((prevState) => ({...prevState, date: ''}));
    }, [filters.userName, filters.field, filters.changer]);


    useEffect(() => {
        addToLocalStorage('bookmark', bookmark);
    }, [bookmark]);


    useEffect(() => {
        console.log(order);
    }, [order]);


    const addFavHandler = (id) => {
        if (!bookmark.find((item) => item === id)) {
            const temp = [...bookmark];
            temp.push(id);
            setBookmark(temp);
        } else {
            let temp = [...bookmark];
            temp = temp.filter((el) => el !== id);
            setBookmark(temp);
        }
    };


    const onUserNameChange = (userName) => {
        setFilters((prevState) => ({
            ...prevState,
            userName,
        }));
    };


    const onDateChange = (date) => {
        const temp = bst.find(date);
        if (temp && temp.children) {
            setState(temp.children);
        } else {
            setState([]);
        }
        setFilters((prevState) => ({
            ...prevState,
            date,
        }));
    };


    const onChangerNameChange = (changer) => {
        setFilters((prevState) => ({
            ...prevState,
            changer: changer,
        }));
    };

    const onFieldChange = (field) => {
        setFilters((prevState) => ({
            ...prevState,
            field,
        }));
    };


    return (
        <div className="body">
            <HeaderInput
                onChangerNameChange={onChangerNameChange}
                onDateChange={onDateChange}
                onFieldChange={onFieldChange}
                onUserNameChange={onUserNameChange}
                filters={filters}
            />
            <DataTable favorits={bookmark} addFavHandler={addFavHandler} data={usersData} order={order}
                       changeOrder={setOrder}/>
        </div>
    );
};

export default Home;
