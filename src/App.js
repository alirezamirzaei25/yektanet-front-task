import React, {lazy, Suspense} from 'react'
import './App.css';
import { Route, Switch, Redirect } from 'react-router-dom'
import { routes } from './routes'
import {Loading} from './components/loading/Loading'

import './App.css';
import Home from './pages/home/Home'

function App() {
  return (
      <div className="App">
        <Suspense fallback={<Loading />}>
          <Switch>
            {
              routes.map((item, index) => (
                  <Route key={item.id} path={item.path} exact={item.exact} render={() => <item.component />} />
              ))
            }

            <Redirect to={"/404"} />
          </Switch>
        </Suspense>

      </div>
  );
}


export default App;
