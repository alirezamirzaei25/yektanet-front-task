export const compareSort = ({sortType, sortField, a, b}) =>
{
    if (sortType === 0)
    {
        return a[sortField] <= b[sortField] ? 1 : -1
    }
    else
    {
        return a[sortField] <= b[sortField] ? -1 : 1
    }
}
