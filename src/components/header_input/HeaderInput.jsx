import React from 'react';
import TextInput from './text_input/TextInput';
import './_style.scss';

const HeaderInput = ({
                           onUserNameChange,
                         onChangerNameChange,
                           onFieldChange,
                           onDateChange,
                           filters,
                       }) => {
    const filterItems = [
        {
            key: 1,
            value: filters.userName,
            lable: 'نام تغییر دهنده',
            placeHolder: 'نام تغییر دهنده  را وارد کنید',
            onChanged: onUserNameChange,
        },
        {
            key: 2,
            value: filters.date,
            lable: 'تاریخ',
            placeHolder: ' تاریخ تغییر را وارد کنید',
            type: 'date',
            onChanged: onDateChange,
        },
        {
            key: 3,
            value: filters.changer,

            lable: ' نام آگهی ',
            placeHolder: 'نام آگهی را وارد کنید',
            onChanged: onChangerNameChange,
        },
        {
            key: 4,
            value: filters.field,
            lable: 'فیلد',
            placeHolder: 'نام فیلد را وارد کنید',
            onChanged: onFieldChange,
        },
    ];
    return (
        <div className="filters-header">
            {filterItems.map((item) => {
                return (
                    <TextInput
                        key={item.key}
                        placeholder={item.placeHolder}
                        label={item.lable}
                        onChanged={item.onChanged}
                        type={item.type}
                        value={item.value}
                    />
                );
            })}
        </div>
    );
};

export default HeaderInput;