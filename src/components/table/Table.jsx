import React, {useRef,useEffect,useState} from 'react';
import TablesBody from './body/TableBody';
import TableHeader from './header/TableHeader';
import './_style.scss';
import {compareSort} from  '../../utils/sort'

const DataTable = ({ addFavHandler, data, favorits, bst ,order,changeOrder }) => {

    const item_numbers = 10;
    const nextBtnRef = useRef();
    const prevBtnRef = useRef();
    const [page, setPage] = useState(0);

    useEffect(() => {
        if (page <= 0 || data.length === 0) {
            prevBtnRef.current.disabled = true;
        } else {
            prevBtnRef.current.disabled = false;
        }
        if (page * item_numbers > data.length || data.length === 0) {
            nextBtnRef.current.disabled = true;

        } else {
            nextBtnRef.current.disabled = false;
        }
    }, [page, data]);



    return (
            <div className="table_body">
                <table role="table">
                    <TableHeader order={order} changeOrder={changeOrder} />

                    <TablesBody
                        bst={bst}
                        favorits={favorits}
                        addFavHandler={addFavHandler}
                        data={data}
                        order={order}
                        item_numbers={item_numbers}
                        page={page}
                    />


                </table>

                <div className="page_control">
                    <button className="next_btn" ref={nextBtnRef} onClick={() => {
                        setPage(page + 1)
                    }}> Next
                    </button>
                    <button className="back_btn" ref={prevBtnRef} onClick={() => {
                        setPage(page - 1)
                    }}> Back
                    </button>

                </div>


            </div>
    );
};

export default DataTable;
