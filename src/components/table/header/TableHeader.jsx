import React from 'react';
import {FaAngleUp, FaAngleDown} from "react-icons/fa";
import {labels} from '../../../constants/data/table';
import './_style.scss';
const TableHeader = ({order , changeOrder}) => {
    return (
        <thead>
        <tr role="row">
            {labels.map((hCell, index) => {
                return (
                    <th  className={hCell.name} key={index} onClick={() => changeOrder({"order": order.order ^ 1, "sortType": hCell.name})}>
                        {hCell.text}
                        {order.sortType == hCell.name ? order.order == 0 ?
                            <FaAngleDown color="white"/> : < FaAngleUp color="white"/> : <div/>}
                    </th>

                );
            })}
        </tr>
        </thead>
    );
};

export default TableHeader;
