import React from 'react';
import {compareSort} from  '../../../utils/sort'
import { FaStar} from "react-icons/fa";
import './_style.scss';

const TablesBody = ({ data, addFavHandler, favorits , order ,page , item_numbers}) => {

    return (
        <tbody>
        {data.sort((a, b) => compareSort({sortType:order.order,sortField: order.sortType, a, b}))
            .map((item, index) => {
                let start = page * item_numbers;
                if (start <= index && index < start + item_numbers) {
                    return (
                        <tr key={item.id} onClick={() => addFavHandler(item.id)}>
                            <td className="id"> {favorits.find((bookmark) => bookmark === item.id) && <FaStar/>} {item.id}   </td>
                            <td className="userName">{item.name}</td>
                            <td className="date">{item.date}</td>
                            <td className="advertName">{item.title}</td>
                            <td className="field">{item.field}</td>
                            <td className="old-value">{item.old_value}</td>
                            <td className="new-value">{item.new_value}</td>
                        </tr>
                    )
                } else {
                    return
                }
            })}
        </tbody>
    );


};

export default TablesBody;
