export const labels = [
    { text: 'شماره', name: 'id' },
    { text: 'نام تغییر دهنده', name: 'name' },
    { text: 'تاریخ', name: 'date' },
    { text: 'نام آگهی', name: 'title' },
    { text: 'فیلد', name: 'field' },
    { text: 'مقدار قدیمی', name: 'old_value' },
    { text: 'مقدار جدید', name: 'new_value' },

];
