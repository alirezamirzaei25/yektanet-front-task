import { lazy } from 'react'

const Home = lazy(() =>
    import("./pages/home/Home").then(({ Home }) => ({
        default: Home,
    }))
);

const NotFound = lazy(() =>
    import("./pages/not_found/NotFound").then(({ NotFound }) => ({
        default: NotFound,
    }))
);

export const routes = [

    {
        id: 1,
        path: "/",
        component: Home,
        exact: true
    },
    {
        id: 2,
        path: "/404",
        component: NotFound,
        exact: true
    }
]